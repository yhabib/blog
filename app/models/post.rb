class Post < ActiveRecord::Base
  # Each post has one or more comments
  # We want that when a post is deleted, all its comments are also deleted
  # Validation of the title and body, in order to be able to create a new Post
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body
end
