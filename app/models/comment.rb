class Comment < ActiveRecord::Base
  # Many to one relations -> post sing, and comments plurar
  # Each comment belongs to a singular post
  # Added validatios to ensure the creation of a Comment with the two fields 
  belongs_to :post
  validates_presence_of :post_id
  validates_presence_of :body
end
